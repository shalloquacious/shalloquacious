#!/usr/bim/perl -w
# ======================================================================
# Copyright (c) 2016 Warr1024 <warr1024@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ======================================================================
use strict;
use warnings;
use Encode qw(decode_utf8);
use Fcntl qw(LOCK_EX);
use HTML::TreeBuilder qw();
use IO::Uncompress::Gunzip qw(gunzip);
use List::Util qw(sum0);
use LWP::UserAgent qw();
use Text::Wrap qw(wrap);

$Text::Wrap::columns = 72;

my $needmax = 100 * 1000 * 1000;
my $dbname  = "samples.db";
my $ccode   = "src/markov_data.h";
my $url     = "https://en.wikipedia.org/wiki/Special:Random";

select STDERR;
$| = 1;

sub webfetch {
	my($url) = @_;

	my $ua = new LWP::UserAgent();
	$ua->agent($ENV{'http_useragent'} || '');
	$ENV{'http_proxy'} and $ua->proxy(['https', 'http'], $ENV{'http_proxy'});
	$ua->protocols_allowed(['http', 'https']);
	$ua->cookie_jar({});

	my $req = HTTP::Request->new('GET' => $url);
	$req->header("Accept-Encoding" => "gzip");

	my $resp = $ua->request($req);
	$resp->is_success() or die("$url: " . $resp->status_line());
	$resp->content_type() =~ m#^text/html(?:;.*)?$#
	  or die("unexpected " . $resp->content_type());

	$resp->content();
}

my $minch = ord("a");
my $pad   = chr($minch - 1);

my @db;
if(open(my $fh, "<", $dbname)) {
	my $raw = do { local $/; <$fh> };
	close($fh);
	@db = unpack("L>*", $raw);
}
while(scalar(@db) < (27 * 27 * 27 * 27)) { push @db, 0; }

sub dbidx {
	my($s) = @_;
	my $x = 0;
	for(my $i = 0; $i < 4; $i++) {
		$x = $x * 27 + ord(substr($s, $i, 1)) - $minch + 1;
	}
	return $x;
}

my $gt = 0;
for my $i (@db) { $gt += $i; }

printf("      %10ds\n", $gt);

while($gt < $needmax) {
	my $txt   = webfetch($url);
	my $bytes = length($txt);
	print(".");

	my $gunz;
	gunzip(\$txt, \$gunz) and $txt = $gunz;
	print(".");

	my $ht = HTML::TreeBuilder->new();
	$ht->parse(decode_utf8($txt));
	$ht->eof();
	$txt = "";
	$ht->look_down(
		_tag => "div",
		id   => "content",
		sub {
			$txt = $_[0]->as_text();
		});
	$txt or die("blank content");
	print(".");

	my @words = grep { m#^[a-z]+$# }
	  map { tr#A-Za-z##cd; lc($_) }
	  grep { !m#\d# }
	  split(m#\s+#, $txt);
	$txt = join("", @words);
	print(".");

	for my $w (@words) {
		$db[dbidx("$pad$pad$pad" . substr($w, 1, 1))]++;
		$gt++;
		if(length($w) >= 2) {
			$db[dbidx("$pad$pad" . substr($w, 1, 2))]++;
			$gt++;
			if(length($w) >= 3) {
				$db[dbidx($pad . substr($w, 1, 3))]++;
				$db[dbidx(substr($w, -3) . $pad)]++;
				$gt += 2;
			}
		}
	}
	print(".");

	for(my $i = 0; $i < length($txt) - 3; $i++) {
		$db[dbidx(substr($txt, $i, 4))]++;
		$gt++;
	}
	print(".");

	open(my $fh, ">", "$dbname.new") or die($!);
	print $fh pack("L>*", @db);
	close($fh);
	rename("$dbname.new", $dbname);

	printf( "%10ds%10dc%10dw%10db\n", $gt,
		length($txt), scalar(@words), $bytes);
}

for(my $i = 0; $i < (27 * 27 * 27); $i++) {
	my $t = 0;
	for(my $j = 0; $j < 27; $j++) {
		$t += $db[$i * 27 + $j];
	}
	$t or next;
	for(my $j = 0; $j < 27; $j++) {
		$db[$i * 27 + $j] /= $t;
	}
}

open(my $cfh, ">", "$ccode.new") or die($!);
while(<DATA>) {
	s#\$0#$0#g;
	m#(.*\{)\.\.\.(\}.*)#
	  and $_ = wrap($1, "  ", join(", ", @db) . $2);
	print $cfh $_;
}
close($cfh);
rename("$ccode.new", $ccode);

__DATA__
/*
 * This C/C++ header file contains code generated automatically
 * by $0.  Any manual changes may be lost if the file
 * is regenerated.  Refer to the source code of $0
 * for further details.
 */

#ifndef __markov_data____
#define __markov_data____

double markov_data[] = {...};

#endif
